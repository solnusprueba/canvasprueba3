# vim: set fileencoding=utf-8 :
from django.contrib import admin

from . import models


class PhvaAdmin(admin.ModelAdmin):

    list_display = (
        'id_phva',
        'nombre_caso',
        'p_que_se_hara',
        'p_como',
        'p_cuando',
        'p_donde',
        'p_quien',
        'p_con_que',
        'p_para_que',
        'h_ident_oportunidad',
        'h_desarrollo_plan',
        'h_implementacion_procesos',
        'h_implementacion_mejoras',
        'v_hizo_lo_planeado',
        'v_logro_resultados',
        'v_que_eficacia',
        'v_que_impacto',
        'v_que_explica_resultados',
        'a_que_aprendimos',
        'a_que_errores',
        'a_que_aciertos',
        'a_como_cuando_aplicar',
    )
    list_filter = ('p_cuando',)


def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.Phva, PhvaAdmin)
