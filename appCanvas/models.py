# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Phva(models.Model):
    id_phva = models.AutoField(primary_key=True)
    nombre_caso = models.CharField(max_length=50)
    p_que_se_hara = models.CharField(max_length=100, blank=True, null=True)
    p_como = models.CharField(max_length=100, blank=True, null=True)
    p_cuando = models.DateField(blank=True, null=True)
    p_donde = models.CharField(max_length=100, blank=True, null=True)
    p_quien = models.CharField(max_length=100, blank=True, null=True)
    p_con_que = models.CharField(max_length=100, blank=True, null=True)
    p_para_que = models.CharField(max_length=100, blank=True, null=True)
    h_ident_oportunidad = models.CharField(max_length=500, blank=True, null=True)
    h_desarrollo_plan = models.CharField(max_length=200, blank=True, null=True)
    h_implementacion_procesos = models.CharField(max_length=200, blank=True, null=True)
    h_implementacion_mejoras = models.CharField(max_length=200, blank=True, null=True)
    v_hizo_lo_planeado = models.NullBooleanField()
    v_logro_resultados = models.NullBooleanField()
    v_que_eficacia = models.CharField(max_length=100, blank=True, null=True)
    v_que_impacto = models.CharField(max_length=100, blank=True, null=True)
    v_que_explica_resultados = models.CharField(max_length=200, blank=True, null=True)
    a_que_aprendimos = models.CharField(max_length=100, blank=True, null=True)
    a_que_errores = models.CharField(max_length=200, blank=True, null=True)
    a_que_aciertos = models.CharField(max_length=200, blank=True, null=True)
    a_como_cuando_aplicar = models.CharField(max_length=100, blank=True, null=True)
    id_proyecto = models.ForeignKey('Proyecto', models.DO_NOTHING, db_column='id_proyecto')

    class Meta:
        managed = False
        db_table = 'phva'


class Proyecto(models.Model):
    id_proyecto = models.AutoField(primary_key=True)
    nombre_proyecto = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'proyecto'
